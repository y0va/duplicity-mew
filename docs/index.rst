.. duplicity documentation master file, created by
   sphinx-quickstart on Wed Jul 31 10:32:30 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to duplicity's documentation!
=====================================

.. toctree:: modules
   :maxdepth: 4
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
